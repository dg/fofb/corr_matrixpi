library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use std.textio.all;

library OSVVM ;
    context OSVVM.OsvvmContext ;
    use osvvm.ScoreboardPkg_slv.all ;

-- We need AXIS and AXI4L
library osvvm_AXI4 ;
    context osvvm_AXI4.AxiStreamContext ;
    context osvvm_Axi4.Axi4LiteContext ;

-- DESYRDL interface package
library desyrdl;
    use desyrdl.pkg_corr_matrix.all;


use work.pkg_corr_matrix.all;


entity TestCtrl is
    port (
        -- Global Signal Interface
        Clk                 : In    std_logic ;
        nReset              : In    std_logic ;

        -- Transaction Interfaces
        StreamTxRec         : inout StreamRecType;
        StreamRxRec         : inout StreamRecType;
        ManagerRec          : inout AddressBusRecType
    ) ;

    -- Derive AXI interface properties from the StreamTxRec
    constant DATA_TX_WIDTH : integer := StreamTxRec.DataToModel'length ;
    --constant DATA_TX_BYTES : integer := DATA_WIDTH/8 ;

    -- Simplifying access to Burst FIFOs using aliases
    --alias TxBurstFifo : ScoreboardIdType is StreamTxRec.BurstFifo ;
    --alias RxBurstFifo : ScoreboardIdType is StreamRxRec.BurstFifo ;

    constant AXI_ADDR_WIDTH : integer := ManagerRec.Address'length ;
    constant AXI_DATA_WIDTH : integer := ManagerRec.DataToModel'length ;

    constant OSVVM_RESULTS_DIR   : string := "" ;
    constant OSVVM_PATH_TO_TESTS : string := "" ;

end entity TestCtrl ;

