#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 11:24:47 2023

@author: broucquart
"""

import pandas as pd
import numpy as np


inputs = pd.read_csv("bpmdata.txt", header=None, sep=' ')
inputs.drop(columns=244, inplace=True)
_c=np.empty(244, dtype=int)
_c[::2] = range(1,123)
_c[1::2] = range(123,245)
inputs.columns = _c


orbrefs = pd.read_csv("reforbit.txt", header=None)[0]
orbrefs.index = orbrefs.index+1


alpha=72

MA = inputs.copy()
MA.loc[-1]=0

for n in range(len(MA)-1): 
    MA.loc[n] = (inputs.loc[n]-orbrefs)*alpha + MA.loc[n-1] - ( (MA.loc[n-1]*alpha).apply(lambda x: np.round(x/2**16,0).astype('int')) )
    

rMA = MA.apply(lambda x: np.round(x/2**16,0).astype('int'))
