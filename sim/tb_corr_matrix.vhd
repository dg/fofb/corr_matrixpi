library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

library osvvm ;
    context osvvm.OsvvmContext ;

library osvvm_AXI4 ;
    context osvvm_AXI4.AxiStreamContext ;
    context osvvm_AXI4.Axi4LiteContext ;

library desyrdl;
    use desyrdl.common.all;
    use desyrdl.pkg_corr_matrix.all;

use work.pkg_corr_matrix.all;

entity tb_corr_matrix is
end entity tb_corr_matrix;

architecture TestHarness of tb_corr_matrix is

    constant TPERIOD_CLK : time := 10 ns ;
    constant TPD         : time := 1 ns ;


    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal tb_clk       : std_logic;
    signal tb_rst_n     : std_logic;

    -- AXI-MM desyrdl
    signal tb_s_axi_m2s       : t_corr_matrix_m2s;
    signal tb_s_axi_s2m       : t_corr_matrix_s2m;

    -- Address Bus Transaction Interface
    signal ManagerRec: AddressBusRecType(
      Address(C_ADDR_WIDTH-1 downto 0),
      DataToModel(C_DATA_WIDTH-1 downto 0),
      DataFromModel(C_DATA_WIDTH-1 downto 0)
    ) ;

    -- AXI Manager Functional Interface
    signal   AxiBus : Axi4LiteRecType(
        WriteAddress( Addr (C_ADDR_WIDTH-1 downto 0) ),
        WriteData   ( Data (C_DATA_WIDTH-1 downto 0),   Strb(C_DATA_WIDTH/8-1 downto 0) ),
        ReadAddress ( Addr (C_ADDR_WIDTH-1 downto 0) ),
        ReadData    ( Data (C_DATA_WIDTH-1 downto 0) )
    ) ;

    -- AXI Stream transmitter Functionnal Interface
    signal tb_axis_tx_tvalid    : std_logic ;
    signal tb_axis_tx_tready    : std_logic ;
    signal tb_axis_tx_tdata     : std_logic_vector(2*C_W_BPMPOS+C_W_BPMID-1 downto 0) ;
    signal tb_axis_tx_tid       : std_logic_vector(0 downto 0) ;
    signal tb_axis_tx_tdest     : std_logic_vector(0 downto 0) ;
    signal tb_axis_tx_tuser     : std_logic_vector(C_W_BPMSEQ-1 downto 0) ;
    signal tb_axis_tx_tstrb     : std_logic_vector(tb_axis_tx_tdata'length/8-1 downto 0) ;
    signal tb_axis_tx_tkeep     : std_logic_vector(tb_axis_tx_tdata'length/8-1 downto 0) ;
    signal tb_axis_tx_tlast     : std_logic ;


    -- AXI Stream transmitter Functionnal Interface
    signal tb_axis_rx_tvalid    : std_logic ;
    signal tb_axis_rx_tready    : std_logic ;
    signal tb_axis_rx_tdata     : std_logic_vector(C_W_COR+C_W_PSCID-1 downto 0) ;
    signal tb_axis_rx_tid       : std_logic_vector(0 downto 0) ;
    signal tb_axis_rx_tdest     : std_logic_vector(0 downto 0) ;
    signal tb_axis_rx_tuser     : std_logic_vector(C_W_BPMSEQ-1 downto 0) ;
    signal tb_axis_rx_tstrb     : std_logic_vector(tb_axis_tx_tdata'length/8-1 downto 0) ;
    signal tb_axis_rx_tkeep     : std_logic_vector(tb_axis_tx_tdata'length/8-1 downto 0) ;
    signal tb_axis_rx_tlast     : std_logic ;

    -- Stream Transaction Interface
    constant C_W_AXIS_RX_PARAM : natural := tb_axis_rx_tid'length + tb_axis_tx_tdest'length + tb_axis_tx_tuser'length +1;
    constant C_W_AXIS_TX_PARAM : natural := tb_axis_tx_tid'length + tb_axis_tx_tdest'length + tb_axis_tx_tuser'length +1;
    signal StreamTxRec, StreamRxRec : StreamRecType(
        DataToModel   (tb_axis_tx_tdata'left downto 0),
        DataFromModel (tb_axis_rx_tdata'left downto 0),
        ParamToModel  (C_W_AXIS_TX_PARAM-1 downto 0),
        ParamFromModel(C_W_AXIS_RX_PARAM-1 downto 0)
    ) ;

    ---------------------------
    -- COMPONENT DECLARATION --
    ---------------------------
    component TestCtrl is
    port (
        -- Global Signal Interface
        Clk             : In    std_logic ;
        nReset          : In    std_logic ;
        -- Transaction Interfaces
        StreamTxRec   : inout StreamRecType;
        StreamRxRec   : inout StreamRecType;
        ManagerRec    : inout AddressBusRecType
    );
    end component TestCtrl ;

begin

    ---------------------
    -- CLOCK AND RESET --
    ---------------------
    Osvvm.TbUtilPkg.CreateClock (
        Clk        => tb_Clk,
        Period     => Tperiod_Clk
    );

    Osvvm.TbUtilPkg.CreateReset (
        Reset       => tb_rst_n,
        ResetActive => '0',
        Clk         => tb_clk,
        Period      => 7 * tperiod_Clk,
        tpd         => tpd
    );

    -----------------------
    -- DUT INSTANCIATION --
    -----------------------
    dut: entity work.top_corr_matrix
    port map(
        clk            => tb_clk,
        rst_n          => tb_rst_n,
        pps            => '0',

        -- AXI-MM interface
        s_axi_m2s      => tb_s_axi_m2s,
        s_axi_s2m      => tb_s_axi_s2m,

        -- AXIS input
        s_axis_tdata   => tb_axis_tx_tdata,
        s_axis_tuser   => tb_axis_tx_tuser,
        s_axis_tvalid  => tb_axis_tx_tvalid,

        -- AXIS output
        m_axis_tdata   => tb_axis_rx_tdata,
        m_axis_tuser   => tb_axis_rx_tuser,
        m_axis_tvalid  => tb_axis_rx_tvalid
    );


    ----------------------
    -- AXI-MM CONNEXION --
    ----------------------
    tb_s_axi_m2s.awaddr(tb_s_axi_m2s.awaddr'left downto C_ADDR_WIDTH)  <= (others => '0');
    tb_s_axi_m2s.awaddr(C_ADDR_WIDTH-1 downto 0)  <= axibus.writeaddress.addr;
    tb_s_axi_m2s.awprot          <= axibus.writeaddress.prot;
    tb_s_axi_m2s.awvalid         <= axibus.writeaddress.valid;
    tb_s_axi_m2s.wdata           <= axibus.writedata.data;
    tb_s_axi_m2s.wstrb           <= axibus.writedata.strb;
    tb_s_axi_m2s.wvalid          <= axibus.writedata.valid;
    tb_s_axi_m2s.bready          <= axibus.writeresponse.ready;
    tb_s_axi_m2s.araddr(tb_s_axi_m2s.araddr'left downto C_ADDR_WIDTH)  <= (others => '0');
    tb_s_axi_m2s.araddr(C_ADDR_WIDTH-1 downto 0)          <= axibus.readaddress.addr;
    tb_s_axi_m2s.arprot          <= axibus.readaddress.prot;
    tb_s_axi_m2s.arvalid         <= axibus.readaddress.valid;
    tb_s_axi_m2s.rready          <= axibus.readdata.ready;
    axibus.writeaddress.ready    <= tb_s_axi_s2m.awready;
    axibus.writedata.ready       <= tb_s_axi_s2m.wready;
    axibus.writeresponse.resp    <= tb_s_axi_s2m.bresp;
    axibus.writeresponse.valid   <= tb_s_axi_s2m.bvalid;
    axibus.readaddress.ready     <= tb_s_axi_s2m.arready;
    axibus.readdata.data         <= tb_s_axi_s2m.rdata;
    axibus.readdata.resp         <= tb_s_axi_s2m.rresp;
    axibus.readdata.valid        <= tb_s_axi_s2m.rvalid;

    -----------------------------
    -- VERIFICATION COMPONENTS --
    -----------------------------
    -- AXI-MM Verification Manager
    vc_axi_manager : Axi4LiteManager
    port map (
        -- Globals
        Clk         => tb_clk,
        nReset      => tb_rst_n,
        -- AXI Manager Functional Interface
        AxiBus      => AxiBus,
        -- Testbench Transaction Interface
        TransRec    => ManagerRec
    ) ;

    -- Axi-Stream Verification Manager
    vc_axis_transmitter : AxiStreamTransmitter
    generic map (
        tperiod_Clk    => tperiod_Clk,
        tpd_Clk_TValid => tpd,
        tpd_Clk_TID    => tpd,
        tpd_Clk_TDest  => tpd,
        tpd_Clk_TUser  => tpd,
        tpd_Clk_TData  => tpd,
        tpd_Clk_TStrb  => tpd,
        tpd_Clk_TKeep  => tpd,
        tpd_Clk_TLast  => tpd
    )
    port map (
        -- Globals
        Clk       => tb_clk,
        nReset    => tb_rst_n,
        -- AXI Stream Interface
        TValid    => tb_axis_tx_tvalid,
        TReady    => tb_axis_tx_tready,
        TID       => tb_axis_tx_tid,
        TDest     => tb_axis_tx_tdest,
        TUser     => tb_axis_tx_tuser,
        TData     => tb_axis_tx_tdata ,
        TStrb     => tb_axis_tx_tstrb,
        TKeep     => tb_axis_tx_tkeep,
        TLast     => tb_axis_tx_tlast,
        -- Testbench Transaction Interface
        TransRec  => StreamTxRec
    );
    tb_axis_tx_tready   <= '1'; -- We do not have a TREADY on DUT

    -- Axi-Stream Verification Manager
    vc_axis_receiver : AxiStreamReceiver
    generic map (
        tperiod_Clk    => tperiod_Clk,
        tpd_Clk_Tready => tpd
    )
    port map (
        -- Globals
        Clk       => tb_clk,
        nReset    => tb_rst_n,
        -- AXI Stream Interface
        TValid    => tb_axis_rx_tvalid,
        TReady    => tb_axis_rx_tready,
        TID       => tb_axis_rx_tid,
        TDest     => tb_axis_rx_tdest,
        TUser     => tb_axis_rx_tuser,
        TData     => tb_axis_rx_tdata ,
        TStrb     => tb_axis_rx_tstrb,
        TKeep     => tb_axis_rx_tkeep,
        TLast     => tb_axis_rx_tlast,
        -- Testbench Transaction Interface
        TransRec  => StreamRxRec
    );


    ---------------
    -- TEST CTRL --
    ---------------
    TestCtrl_1 : TestCtrl
    port map (
        -- Globals
        Clk            => tb_clk,
        nReset         => tb_rst_n,

        -- Testbench Transaction Interfaces
        StreamTxRec    => StreamTxRec,
        StreamRxRec    => StreamRxRec,
        ManagerRec     => ManagerRec
    ) ;


end architecture TestHarness;


