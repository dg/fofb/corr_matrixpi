use work.pkg_corr_matrix_version.all;

architecture basic of TestCtrl is


    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    constant C_NUMBPM : positive := 122;
    signal ConfigDone : integer_barrier := 1 ;
    signal TestDone : integer_barrier := 1 ;

    --------------------------
    -- FUNCTION DECLARATION --
    --------------------------
    function f_addr(addr:natural) return std_logic_vector is
    begin
        return std_logic_vector(to_unsigned(addr, 32));
    end function;

    function f_sdata(data:integer) return std_logic_vector is
    begin
        return std_logic_vector(to_signed(data, 32));
    end function;

    function f_data(data:natural) return std_logic_vector is
    begin
        return std_logic_vector(to_unsigned(data, 32));
    end function;

    function f_bpmpkt(id:natural; x:integer; y:integer) return std_logic_vector is
    begin
        return std_logic_vector(to_unsigned(id, C_W_BPMID)) & std_logic_vector(to_signed(y, C_W_BPMPOS)) & std_logic_vector(to_signed(x, C_W_BPMPOS));
    end function;


begin

    ------------------------------------------------------------
    -- ControlProc
    --   Set up AlertLog and wait for end of test
    ------------------------------------------------------------
    ControlProc : process
    begin
        -- Initialization of test
        SetAlertLogName("BasicTest");
        SetLogEnable(PASSED, TRUE);    -- Enable PASSED logs
        SetLogEnable(INFO, TRUE);    -- Enable INFO logs

        -- Wait for testbench initialization
        wait for 0 ns ;  wait for 0 ns;
        TranscriptOpen(OSVVM_RESULTS_DIR & "BasicTestTr.txt");
        SetTranscriptMirror(TRUE);

        -- Wait for Design Reset
        wait until nReset = '1';
        ClearAlerts;

        -- Wait for test to finish
        WaitForBarrier(TestDone, 35 ms);
        AlertIf(now >= 35 ms, "Test finished due to timeout");
        AlertIf(GetAffirmCount < 1, "Test is not Self-Checking");


        TranscriptClose;
        EndOfTestReports;
        std.env.stop;
        wait;
    end process ControlProc;


    ------------------------------------------------------------
    -- ManagerProc
    --   Generate transactions for AxiManager
    ------------------------------------------------------------
    ManagerProc : process
        variable Data : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) ;
        file read_file : text;
        variable line_v : line;
        variable int_v : integer;

    begin
        wait until nReset = '1';
        WaitForClock(ManagerRec, 2);

        log("Read version", INFO) ;
        Read(ManagerRec, f_addr(4), Data) ;
        AffirmIfEqual(Data, C_VERSION, "Manager Read Data: ") ;

        log("==--- Configure the DUT ---==", INFO);
        log("+-- Global Config", INFO);

        -- Correction coefficients
        Write(ManagerRec, f_addr(16#18#), f_sdata(256));
        Write(ManagerRec, f_addr(16#1C#), f_sdata(0));
        Write(ManagerRec, f_addr(16#20#), f_sdata(64));
        Write(ManagerRec, f_addr(16#24#), f_sdata(-16300));
        Write(ManagerRec, f_addr(16#38#), f_sdata(256));
        Write(ManagerRec, f_addr(16#3C#), f_sdata(0));
        Write(ManagerRec, f_addr(16#40#), f_sdata(64));
        Write(ManagerRec, f_addr(16#44#), f_sdata(-16300));

        Write(ManagerRec, f_addr(16#28#), f_sdata(32768));
        Write(ManagerRec, f_addr(16#2C#), f_sdata(0));
        Write(ManagerRec, f_addr(16#30#), f_sdata(8192));
        Write(ManagerRec, f_addr(16#34#), f_sdata(0));
        Write(ManagerRec, f_addr(16#48#), f_sdata(32768));
        Write(ManagerRec, f_addr(16#4C#), f_sdata(0));
        Write(ManagerRec, f_addr(16#50#), f_sdata(8192));
        Write(ManagerRec, f_addr(16#54#), f_sdata(0));

        -- Num BPM
        Write(ManagerRec, f_addr(16#58#), f_data(C_NUMBPM));

        -- Rst corr and threshold
        Write(ManagerRec, f_addr(16#8#), f_sdata(10));
        Write(ManagerRec, f_addr(16#C#), f_sdata(10));

        -- Enable
        Write(ManagerRec, f_addr(16#8#), f_sdata(1));
        Write(ManagerRec, f_addr(16#C#), f_sdata(1));

        log("+-- Writing orbit reference...", INFO);

        -- Load reference orbit from file
        file_open(read_file, "reforbit.txt", read_mode);
        -- X ref orbit
        for I in 0 to C_N_MM_BPM-1 loop
            readline(read_file, line_v);
            read(line_v, int_v);
            Write(ManagerRec, f_addr(16#400#+I*4), f_sdata(int_v));
        end loop;
        -- Y ref orbit
        for I in 0 to C_N_MM_BPM-1 loop
            readline(read_file, line_v);
            read(line_v, int_v);
            Write(ManagerRec, f_addr(16#800#+I*4), f_sdata(int_v));
        end loop;
        file_close(read_file);

        log("+-- Writing PSCID...", INFO);
        for J in 0 to C_N_MM_PSC-1 loop
            Write(ManagerRec, f_addr(16#C00#+J*4), f_sdata(J+1));
        end loop;

        log("+-- Writing inv. resp. matrix coefficients...", INFO);

        -- Set Matrix Coefs
        file_open(read_file, "respmat.txt", read_mode);
        for J in 0 to C_N_MM_PSC-1 loop
            for I in 0 to C_N_MM_BPM-1 loop
                readline(read_file, line_v);
                read(line_v, int_v);
                Write(ManagerRec, f_addr(16#1000#+J*1024+I*4), f_sdata(int_v));
            end loop;
        end loop;

        log("+-- Configuration done", INFO);
        WaitForBarrier(ConfigDone);

        -- Global Enable
        WaitForClock(ManagerRec, 10) ;
        --Write(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_CONFIG_ID).address), X"00000001") ;

        wait ;
    end process ManagerProc ;

    ------------------------------------------------------------
    -- AxiTransmitterProc
    --   Generate transactions for AxiTransmitter
    ------------------------------------------------------------
    TransmitterProc : process
        file read_file : text;
        variable line_v : line;
        variable intx_v : integer;
        variable inty_v : integer;
        variable nturn : natural := 0;

    begin
        wait until nReset = '1' ;
        WaitForClock(StreamTxRec, 2) ;

        -- Load data input file
        file_open(read_file, "bpmdata.txt", read_mode);

        WaitForBarrier(ConfigDone) ;

        log("+-- Sending bpm packets", INFO);
        --while (not endfile(read_file)) loop
        while nturn <  1500 loop
            readline(read_file, line_v);

            for I in 0 to C_NUMBPM-1 loop
                read(line_v, intx_v);
                read(line_v, inty_v);

                Send(StreamTxRec, f_bpmpkt(I, intx_v, inty_v), std_logic_vector(to_unsigned(nturn,8)&'0'));
            end loop;
            nturn := nturn+1;

            -- Simulate interpacket delay
            WaitForClock(StreamTxRec, 150) ;
        end loop;
        file_close(read_file);

        WaitForBarrier(TestDone) ;
        wait ;
    end process TransmitterProc ;


    ------------------------------------------------------------
    -- AxiReceiverProc
    --   Generate transactions for AxiReceiver
    ------------------------------------------------------------
    ReceiverProc : process
        variable ExpData, RxData : std_logic_vector(C_W_COR+C_W_PSCID-1 downto 0);
        file read_file : text;
        variable line_v : line;
        variable int_v : integer;

    begin
        wait until nReset = '1' ;
        WaitForClock(StreamRxRec, 2) ;

        -- Load data input file
        file_open(read_file, "corrout.txt", read_mode);

        -- Check output
        while true loop
            readline(read_file, line_v);
            for J in 0 to C_N_MM_PSC-1 loop
                Get(StreamRxRec, RxData) ;

                read(line_v, int_v);

                ExpData := std_logic_vector(to_unsigned(J+1, C_W_PSCID)) & std_logic_vector(to_signed(int_v, C_W_COR));

                wait for 0 ns;
                AffirmIfEqual(RxData, ExpData, "");

            end loop;
        end loop;

        wait;
    end process ReceiverProc;

end basic;

Configuration tc_basic of tb_corr_matrix is
  for TestHarness
    for TestCtrl_1 : TestCtrl
      use entity work.TestCtrl(basic);
    end for;
  end for;
end tc_basic;

