library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.pkg_corr_matrix.t_mem_REFORBITX_out;
use desyrdl.pkg_corr_matrix.t_mem_REFORBITX_in;
use desyrdl.pkg_corr_matrix.t_mem_REFORBITY_out;
use desyrdl.pkg_corr_matrix.t_mem_REFORBITY_in;

library desy;
use desy.ram_tdp;
use desy.math_signed.all;

use work.pkg_corr_matrix.all;

entity orbit_error is
    port(
        clk                    : in std_logic;
        rst_n                  : in std_logic;

        -- RefOrbit table, desyrdl
        reforbitx_i            : in t_mem_REFORBITX_out;
        reforbitx_o            : out t_mem_REFORBITX_in;
        reforbity_i            : in t_mem_REFORBITY_out;
        reforbity_o            : out t_mem_REFORBITY_in;

        -- BPM position input
        bpm_x                  : in signed(C_W_BPMPOS-1 downto 0);
        bpm_y                  : in signed(C_W_BPMPOS-1 downto 0);
        bpm_id                 : in std_logic_vector(C_W_BPMID-1 downto 0);
        bpm_seq                : in std_logic_vector(C_W_BPMSEQ-1 downto 0);
        bpm_tvalid             : in std_logic;

        -- Orbit error output
        errbpm_x               : out signed(C_W_OE-1 downto 0);
        errbpm_y               : out signed(C_W_OE-1 downto 0);
        errbpm_id              : out std_logic_vector(C_W_BPMID-1 downto 0);
        errbpm_seq             : out std_logic_vector(C_W_BPMSEQ-1 downto 0);
        errbpm_tvalid          : out std_logic
    );
end entity orbit_error;

architecture rtl of orbit_error is

    type arr_slv is array (natural range <>) of std_logic_vector;
    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal table_refx       : std_logic_vector(C_W_BPMPOS-1 downto 0);
    signal table_refy       : std_logic_vector(C_W_BPMPOS-1 downto 0);

    signal r_bpm_x          : signed(C_W_BPMPOS-1 downto 0);
    signal r_bpm_y          : signed(C_W_BPMPOS-1 downto 0);
    signal r_bpm_id         : arr_slv(0 to 1)(C_W_BPMID-1 downto 0);
    signal r_bpm_seq        : arr_slv(0 to 1)(C_W_BPMSEQ-1 downto 0);
    signal r_tvalid         : std_logic_vector(1 downto 0);

    signal pi_table_refx_en : std_logic;
    signal pi_table_refy_en : std_logic;
    signal pi_table_refx_we : std_logic;
    signal pi_table_refy_we : std_logic;

begin

    -------------------------------
    -- POSITION REFERENCE TABLES --
    -------------------------------
    -- Port A is read write from AXI controller, Port B is read only from logic
    inst_refx_table: entity desy.ram_tdp
    generic map(
        G_ADDR      => C_W_MM_IDCNT,
        G_DATA      => C_W_BPMPOS
    )
    port map(
        pi_clk_a    => clk,
        pi_en_a     => reforbitx_i.en,
        pi_we_a     => reforbitx_i.we,
        pi_addr_a   => reforbitx_i.addr(C_W_MM_IDCNT-1 downto 0),
        pi_data_a   => reforbitx_i.data,
        po_data_a   => reforbitx_o.data,
        pi_clk_b    => clk,
        pi_en_b     => '1',
        pi_we_b     => '0',
        pi_addr_b   => bpm_id(C_W_MM_IDCNT-1 downto 0),
        pi_data_b   => (others => '0'),
        po_data_b   => table_refx
    );

    inst_refy_table: entity desy.ram_tdp
    generic map(
        G_ADDR      => C_W_MM_IDCNT,
        G_DATA      => C_W_BPMPOS
    )
    port map(
        pi_clk_a    => clk,
        pi_en_a     => reforbity_i.en,
        pi_we_a     => reforbity_i.we,
        pi_addr_a   => reforbity_i.addr(C_W_MM_IDCNT-1 downto 0),
        pi_data_a   => reforbity_i.data,
        po_data_a   => reforbity_o.data,
        pi_clk_b    => clk,
        pi_en_b     => '1',
        pi_we_b     => '0',
        pi_addr_b   => bpm_id(C_W_MM_IDCNT-1 downto 0),
        pi_data_b   => (others => '0'),
        po_data_b   => table_refy
    );

    -----------------------
    -- ERROR SUBSTRACTOR --
    -----------------------
    p_sub_err:process(clk, rst_n)
    begin
        if rst_n = '0' then
            errbpm_x    <= (others => '0');
            errbpm_y    <= (others => '0');
        elsif rising_edge(clk) then
            errbpm_x    <= f_resize_sat(f_diff_sat(r_bpm_x, signed(table_refx)), C_W_OE);
            errbpm_y    <= f_resize_sat(f_diff_sat(r_bpm_y, signed(table_refy)), C_W_OE);
        end if;
    end process;

    ---------------------
    -- DELAY REGISTERS --
    ---------------------
    p_delay_reg:process(clk, rst_n)
    begin
        if rst_n = '0' then
            r_bpm_id    <= (others => (others => '0'));
            r_bpm_seq   <= (others => (others => '0'));
            r_tvalid    <= (others => '0');
            r_bpm_x     <= (others => '0');
            r_bpm_y     <= (others => '0');
        elsif rising_edge(clk) then
            r_bpm_id(0)     <= bpm_id;
            for I in 1 to r_bpm_id'right loop
                r_bpm_id(I) <= r_bpm_id(I-1);
            end loop;

            r_bpm_seq(0)    <= bpm_seq;
            for I in 1 to r_bpm_seq'right loop
                r_bpm_seq(I) <= r_bpm_seq(I-1);
            end loop;

            r_bpm_x <= bpm_x;
            r_bpm_y <= bpm_y;

            r_tvalid    <= r_tvalid(r_tvalid'left-1 downto 0) & bpm_tvalid;
        end if;
    end process;

    ----------------------
    -- OUTPUT CONNEXION --
    ----------------------
    errbpm_id       <= r_bpm_id(r_bpm_id'right);
    errbpm_seq      <= r_bpm_seq(r_bpm_seq'right);
    errbpm_tvalid   <= r_tvalid(r_tvalid'left);

end architecture;

