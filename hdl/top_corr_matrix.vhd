library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.pkg_corr_matrix.all;

use work.pkg_corr_matrix_version.all;
use work.pkg_corr_matrix.all;

entity top_corr_matrix is
    port(
        clk            : in std_logic;
        rst_n          : in std_logic;

        pps            : in std_logic;

        -- AXI-MM interface
        s_axi_m2s      : in t_corr_matrix_m2s;
        s_axi_s2m      : out t_corr_matrix_s2m;

        -- AXIS input
        s_axis_tdata   : in std_logic_vector(2*C_W_BPMPOS+C_W_BPMID-1 downto 0);
        s_axis_tuser   : in std_logic_vector(C_W_BPMSEQ-1 downto 0);
        s_axis_tvalid  : in std_logic;

        -- AXIS output
        m_axis_tdata   : out std_logic_vector(C_W_COR+C_W_PSCID-1 downto 0);
        m_axis_tuser   : out std_logic_vector(C_W_BPMSEQ-1 downto 0);
        m_axis_tvalid  : out std_logic
    );
end entity;

architecture struct of top_corr_matrix is

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal rst          : std_logic;

    signal mm_l2a       : t_addrmap_corr_matrix_in;
    signal mm_a2l       : t_addrmap_corr_matrix_out;

    -- unpacked input
    signal bpm_x        : signed(C_W_BPMPOS-1 downto 0);
    signal bpm_y        : signed(C_W_BPMPOS-1 downto 0);
    signal bpm_id       : std_logic_vector(C_W_BPMID-1 downto 0);
    signal bpm_seq      : std_logic_vector(C_W_BPMSEQ-1 downto 0);

    -- Position error
    signal errbpm_x               : signed(C_W_OE-1 downto 0);
    signal errbpm_y               : signed(C_W_OE-1 downto 0);
    signal errbpm_id              : std_logic_vector(C_W_BPMID-1 downto 0);
    signal errbpm_seq             : std_logic_vector(C_W_BPMSEQ-1 downto 0);
    signal errbpm_tvalid          : std_logic;

    -- Matrix mutliplication result
    signal matmult                : signed_array(0 to C_N_MM_PSC-1)(C_W_MM-1 downto 0);
    signal matmult_tvalid         : std_logic;
    signal matmult_seq            : std_logic_vector(C_W_BPMSEQ-1 downto 0);

    -- Corrector result, parallel
    signal corrfirst_valid : std_logic;
    signal corrfirst_seq   : std_logic_vector(C_W_BPMSEQ-1 downto 0);
    signal corrfirst       : signed_array(0 to C_N_MM_PSC-1)(C_W_COR-1 downto 0);
    signal corrfirst_resize: signed_array(0 to C_N_MM_PSC-1)(C_W_MM-1 downto 0);
    signal corrout_valid   : std_logic;
    signal corrout_seq     : std_logic_vector(C_W_BPMSEQ-1 downto 0);
    signal corrout         : signed_array(0 to C_N_MM_PSC-1)(C_W_COR-1 downto 0);
    signal enable_corr_x   : std_logic;
    signal enable_corr_y   : std_logic;

    -- Serializer
    signal overrun_flag     : std_logic;
    signal ser_tdata        : std_logic_vector(C_W_COR+C_W_PSCID-1 downto 0);
    signal ser_tuser        : std_logic_vector(C_W_BPMSEQ-1 downto 0);
    signal ser_tvalid       : std_logic;

    -- Threshold
    signal thresh_reached   : std_logic;


begin

    rst <= not rst_n;

    ----------------------
    -- AXI-MM INTERFACE --
    ----------------------
    inst_aximm: entity desyrdl.corr_matrix
    port map(
        pi_clock => clk,
        pi_reset => rst,
        -- TOP subordinate memory mapped interface
        pi_s_top   => s_axi_m2s,
        po_s_top   => s_axi_s2m,
        -- to logic interface
        pi_addrmap => mm_l2a,
        po_addrmap => mm_a2l
    );

    mm_l2a.VERSION.data.data <= C_VERSION;

    ------------------
    -- UNPACK INPUT --
    ------------------
    bpm_x       <= signed(s_axis_tdata(C_W_BPMPOS-1 downto 0));
    bpm_y       <= signed(s_axis_tdata(2*C_W_BPMPOS-1 downto C_W_BPMPOS));
    bpm_id      <= s_axis_tdata(C_W_BPMID+2*C_W_BPMPOS-1 downto 2*C_W_BPMPOS);
    bpm_seq     <= s_axis_tuser(C_W_BPMSEQ-1 downto 0);


    ------------------------------
    -- ORBIT ERROR TO REFERENCE --
    ------------------------------
    inst_orbit_error: entity work.orbit_error
    port map(
        clk                    => clk,
        rst_n                  => rst_n,

        -- RefOrbit table, desyrdl
        reforbitx_i            => mm_a2l.REFORBITX,
        reforbitx_o            => mm_l2a.REFORBITX,
        reforbity_i            => mm_a2l.REFORBITY,
        reforbity_o            => mm_l2a.REFORBITY,

        -- BPM position input
        bpm_x                  => bpm_x,
        bpm_y                  => bpm_y,
        bpm_id                 => bpm_id,
        bpm_seq                => bpm_seq,
        bpm_tvalid             => s_axis_tvalid,

        -- Orbit error output
        errbpm_x               => errbpm_x,
        errbpm_y               => errbpm_y,
        errbpm_id              => errbpm_id,
        errbpm_seq             => errbpm_seq,
        errbpm_tvalid          => errbpm_tvalid
    );

    -------------------------
    -- ORBIT ERROR AVERAGE --
    -------------------------
    inst_orbit_error_avg_x: entity work.moving_average
    generic map(
        G_W_ID          => C_W_MM_IDCNT,
        G_W_SIGNAL      => C_W_OE,
        G_W_ALPHA       => C_W_ALPHA
    )
    port map(
        clk             => clk,
        rst_n           => rst_n,

        -- Moving average alpha
        alpha           => C_ALPHA_OE,

        -- Signal input
        sig_data        => errbpm_x,
        sig_id          => errbpm_id(C_W_MM_IDCNT-1 downto 0),
        sig_valid       => errbpm_tvalid,

        -- AXI-MM Average signal table
        asigt_en        => mm_a2l.ERRORBITX.en,
        asigt_addr      => mm_a2l.ERRORBITX.addr(C_W_MM_IDCNT-1 downto 0),
        asigt_rdata     => mm_l2a.ERRORBITX.data(C_W_OE-1 downto 0)
    );
    mm_l2a.ERRORBITX.data(31 downto C_W_OE) <= (others => mm_l2a.ERRORBITX.data(C_W_OE-1));

    inst_orbit_error_avg_y: entity work.moving_average
    generic map(
        G_W_ID          => C_W_MM_IDCNT,
        G_W_SIGNAL      => C_W_OE,
        G_W_ALPHA       => C_W_ALPHA
    )
    port map(
        clk             => clk,
        rst_n           => rst_n,

        -- Moving average alpha
        alpha           => C_ALPHA_OE,

        -- Signal input
        sig_data        => errbpm_y,
        sig_id          => errbpm_id(C_W_MM_IDCNT-1 downto 0),
        sig_valid       => errbpm_tvalid,

        -- AXI-MM Average signal table
        asigt_en        => mm_a2l.ERRORBITY.en,
        asigt_addr      => mm_a2l.ERRORBITY.addr(C_W_MM_IDCNT-1 downto 0),
        asigt_rdata     => mm_l2a.ERRORBITY.data(C_W_OE-1 downto 0)
    );
    mm_l2a.ERRORBITY.data(31 downto C_W_OE) <= (others => mm_l2a.ERRORBITY.data(C_W_OE-1));


    ---------------------------
    -- MATRIX MULTIPLICATION --
    ---------------------------
    inst_matrix_mul: entity work.matrix_mul
    port map(
        clk                    => clk,
        rst_n                  => rst_n,

        -- Coef table, desyrdl
        mm_coef_i              => mm_a2l.MATRIXCOEF,
        mm_coef_o              => mm_l2a.MATRIXCOEF,
        id_cnt_load            => mm_a2l.MM_ID_CNT.data.data(C_W_MM_IDCNT-1 downto 0),

        -- Position data in
        pos_x                  => errbpm_x,
        pos_y                  => errbpm_y,
        pos_id                 => errbpm_id,
        pos_seq                => errbpm_seq,
        pos_tvalid             => errbpm_tvalid,

        pps                    => pps,
        mult_rate              => mm_l2a.MULT_RATE.data.data,

        -- Data out
        matmult                => matmult,
        matmult_tvalid         => matmult_tvalid,
        matmult_seq            => matmult_seq
    );

    ------------------
    -- LL CORRECTOR --
    ------------------
    inst_corr: entity work.corr_ll
    port map(
        clk             => clk,
        rst_n           => rst_n,

        -- matmult input
        matmult         => matmult,
        matmult_valid   => matmult_tvalid,
        matmult_seq     => matmult_seq,

        -- Corr coefs
        coef_a_x        => signed(mm_a2l.CORR_K1A_X.data.data),
        coef_b_x        => signed(mm_a2l.CORR_K1B_X.data.data),
        coef_ic_x       => signed(mm_a2l.CORR_K1IC_X.data.data),
        coef_d_x        => signed(mm_a2l.CORR_K1D_X.data.data),
        coef_a_y        => signed(mm_a2l.CORR_K1A_Y.data.data),
        coef_b_y        => signed(mm_a2l.CORR_K1B_Y.data.data),
        coef_ic_y       => signed(mm_a2l.CORR_K1IC_Y.data.data),
        coef_d_y        => signed(mm_a2l.CORR_K1D_Y.data.data),

        reset_corr_x    => mm_a2l.CONTROL_X.RST_CORR.data(0),
        enable_corr_x   => enable_corr_x,
        reset_corr_y    => mm_a2l.CONTROL_Y.RST_CORR.data(0),
        enable_corr_y   => enable_corr_y,

        -- Corr output
        corrout_valid   => corrfirst_valid,
        corrout_seq     => corrfirst_seq,
        corrout         => corrfirst
    );

    gen_resize:for I in 0 to C_N_MM_PSC-1 generate
        corrfirst_resize(I) <= resize(corrfirst(I), C_W_MM);
    end generate;

    inst_corr2: entity work.corr_ll
    port map(
        clk             => clk,
        rst_n           => rst_n,

        -- matmult input
        matmult         => corrfirst_resize,
        matmult_valid   => corrfirst_valid,
        matmult_seq     => corrfirst_seq,

        -- Corr coefs
        coef_a_x        => signed(mm_a2l.CORR_K2A_X.data.data),
        coef_b_x        => signed(mm_a2l.CORR_K2B_X.data.data),
        coef_ic_x       => signed(mm_a2l.CORR_K2IC_X.data.data),
        coef_d_x        => signed(mm_a2l.CORR_K2D_X.data.data),
        coef_a_y        => signed(mm_a2l.CORR_K2A_Y.data.data),
        coef_b_y        => signed(mm_a2l.CORR_K2B_Y.data.data),
        coef_ic_y       => signed(mm_a2l.CORR_K2IC_Y.data.data),
        coef_d_y        => signed(mm_a2l.CORR_K2D_Y.data.data),

        reset_corr_x    => mm_a2l.CONTROL_X.RST_CORR.data(0),
        enable_corr_x   => enable_corr_x,
        reset_corr_y    => mm_a2l.CONTROL_Y.RST_CORR.data(0),
        enable_corr_y   => enable_corr_y,

        -- Corr output
        corrout_valid   => corrout_valid,
        corrout_seq     => corrout_seq,
        corrout         => corrout
    );

    enable_corr_x  <= mm_a2l.CONTROL_X.ENABLE_CORR.data(0) and not thresh_reached;
    enable_corr_y  <= mm_a2l.CONTROL_Y.ENABLE_CORR.data(0) and not thresh_reached;

    ---------------------
    -- DATA SERIALIZER --
    ---------------------
    inst_data_serializer: entity work.data_serializer
    port map(
        clk             => clk,
        rst_n           => rst_n,

        -- PSCID memory
        pscid_table_i   => mm_a2l.PSCIDTABLE,
        pscid_table_o   => mm_l2a.PSCIDTABLE,

        -- Status
        overrun         => overrun_flag,

        -- Corr parallel input
        corrout_valid   => corrout_valid,
        corrout_seq     => corrout_seq,
        corrout         => corrout,

        -- AXIS serial output
        m_axis_tdata    => ser_tdata,
        m_axis_tuser    => ser_tuser,
        m_axis_tvalid   => ser_tvalid
    );

    m_axis_tdata    <= ser_tdata;
    m_axis_tuser    <= ser_tuser;
    m_axis_tvalid   <= ser_tvalid;

    -------------------------------
    -- CORRECTOR COMMAND AVERAGE --
    -------------------------------
    inst_corrector_command_avg: entity work.moving_average
    generic map(
        G_W_ID          => C_W_PSCID,
        G_W_SIGNAL      => C_W_COR,
        G_W_ALPHA       => C_W_ALPHA
    )
    port map(
        clk             => clk,
        rst_n           => rst_n,

        -- Moving average alpha
        alpha           => C_ALPHA_CC,

        -- Signal input
        sig_data        => signed(ser_tdata(C_W_COR-1 downto 0)),
        sig_id          => ser_tdata(C_W_PSCID+C_W_COR-1 downto C_W_COR),
        sig_valid       => ser_tvalid,

        -- AXI-MM Average signal table
        asigt_en        => mm_a2l.CORRCMD.en,
        asigt_addr      => mm_a2l.CORRCMD.addr(C_W_PSCID-1 downto 0),
        asigt_rdata     => mm_l2a.CORRCMD.data(C_W_COR-1 downto 0)
    );
    mm_l2a.CORRCMD.data(31 downto C_W_COR ) <= (others => mm_l2a.CORRCMD.data(C_W_COR-1));

    --------------------------
    -- THRESHOLD LEVEL COMP --
    --------------------------
    inst_thresh: entity work.lvl_threshold
    generic map(
        G_N_BTH     => 10  -- 99.9 % of range
    )
    port map(
        clk         => clk,
        rst_n       => rst_n,

        s_axis_tdata_cor    => ser_tdata(C_W_COR-1 downto 0),
        s_axis_tvalid       => ser_tvalid,

        enable_thresh       => mm_a2l.THRESH.ENABLE.data(0),
        rst_thresh          => mm_a2l.THRESH.RST.data(0),
        thresh_reached      => thresh_reached
    );


    mm_l2a.STATUS.THRESH_REACHED.data(0) <= thresh_reached;




end architecture;
