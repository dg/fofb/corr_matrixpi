library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.log2;
use ieee.math_real.ceil;
use ieee.numeric_std.all;


package pkg_corr_matrix is


    --------------------------
    -- CONSTANT DECLARATION --
    --------------------------

    -- Data input
    constant C_W_BPMPOS         : natural := 32;
    constant C_W_BPMID          : natural := 16;
    constant C_W_BPMSEQ         : natural := 8;
    constant C_W_PSCID          : natural := 8;

    -- Orbit error
    constant C_N_OE_SAT         : natural := 8;
    constant C_W_OE             : natural := 24; -- C_W_BPMPOS-C_N_OE_SAT;

    -- Matrix multiplier
    constant C_N_MM_BPM         : natural := 122; -- number of bpm (matrix columns)
    constant C_N_MM_PSC         : natural := 100; -- number of psc (matrix lines)

    constant C_W_MM_COEF        : natural := 26;

    constant C_W_MM_MULT        : natural := 50; -- C_W_OE+C_W_MM_COEF;
    constant C_W_MM_IDCNT       : natural := 8;  -- > natural(ceil(log2(real(C_N_MM_BPM))));
    constant C_W_MM_ACCU        : natural := 58; -- C_W_MM_MULT+C_W_MM_IDCNT;

    constant C_N_MM_SAT         : natural := 7;
    constant C_N_MM_RND         : natural := 15;
    constant C_W_MM             : natural := 36; --C_W_MM_ACCU-C_N_MM_SAT-C_N_MM_RND

    -- PI corrector
    constant C_W_COR_COEF       : natural := 17;
    constant C_W_COR_SUMSAT     : natural := 36;
    constant C_N_COR_RND        : natural := 20;

    constant C_W_COR            : natural := 16;

    -- Serializer
    constant C_W_SER_CNT        : natural := 7; -- natural(ceil(log2(real(C_N_MM_PSC))));

    -- Moving average
    constant C_W_ALPHA          : natural := 16;
    constant C_ALPHA_OE         : signed(C_W_ALPHA-1 downto 0) := x"0048"; -- 72 dec
    constant C_ALPHA_CC         : signed(C_W_ALPHA-1 downto 0) := x"0048"; -- 72 dec

    ----------------------
    -- TYPE DECLARATION --
    ----------------------
    type signed_array is array (natural range <>) of signed;


end package;


