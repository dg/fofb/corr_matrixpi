library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.and_reduce;
use ieee.std_logic_misc.or_reduce;

use work.pkg_corr_matrix.all;

entity lvl_threshold is
    generic(
        G_N_BTH             : natural
    );
    port(
        clk                 : in std_logic;
        rst_n               : in std_logic;

        s_axis_tdata_cor    : in std_logic_vector(C_W_COR-1 downto 0);
        s_axis_tvalid       : in std_logic;

        enable_thresh       : in std_logic;
        rst_thresh          : in std_logic;
        thresh_reached      : out std_logic
    );
end entity lvl_threshold;


architecture rtl of lvl_threshold is

    signal sign         : std_logic;
    signal thresh_and   : std_logic;
    signal thresh_nor   : std_logic;

begin

    sign        <= s_axis_tdata_cor(C_W_COR-1);
    thresh_and  <= and_reduce(s_axis_tdata_cor(C_W_COR-2 downto C_W_COR-G_N_BTH-1));
    thresh_nor  <= not or_reduce(s_axis_tdata_cor(C_W_COR-2 downto C_W_COR-G_N_BTH-1));

    p_main:process(clk, rst_n)
    begin

        if rst_n = '0' then
            thresh_reached <= '0';

        elsif rising_edge(clk) then
            if rst_thresh = '1' then
                thresh_reached <= '0';
            else
                if s_axis_tvalid = '1' and enable_thresh = '1' then
                    if  sign = '1'then
                        -- negative
                        if thresh_nor = '1' then
                            thresh_reached <= '1';
                        end if;
                    else
                        -- positive
                        if thresh_and = '1' then
                            thresh_reached <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end architecture;

