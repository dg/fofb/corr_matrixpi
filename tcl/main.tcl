################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {
    variable Config

    # Parse configuration from VHDL package
    parseVhdlConfigFile Config "../hdl/pkg_corrmatrix.vhd"
}

# ==============================================================================
proc setSources {} {
  variable Sources

  # Generate VHDL package with modversion
  genModVerFile VHDL ../hdl/pkg_corr_matrix_version.vhd

  lappend Sources {"../hdl/pkg_corr_matrix_version.vhd" "VHDL"}
  lappend Sources {"../hdl/pkg_corrmatrix.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/corr_ll.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/lvl_threshold.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/matrix_mul.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/orbit_error.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/data_serializer.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/moving_average.vhd" "VHDL 2008"}
  lappend Sources {"../hdl/top_corr_matrix.vhd" "VHDL 2008"}
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/memory/ram/ram_tdp.vhd" "VHDL 2008" "desy"]
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/math/pkg_math_utils.vhd" "VHDL 2008" "desy"]
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/math/pkg_math_signed.vhd" "VHDL 2008" "desy"]
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/common/pkg_common_logic_utils.vhd" "VHDL 2008" "desy"]

  # Simulation sources
  lappend Sources {"../sim/tb_corr_matrix.vhd"  "VHDL 2008" "" "simulation"}
  lappend Sources {"../sim/TestCtrl_e.vhd"  "VHDL 2008" "" "simulation"}
  lappend Sources {"../sim/tc_basic.vhd"  "VHDL 2008" "" "simulation"}
}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace
    addAddressSpace AddressSpace "corr_matrix" RDL {} ../rdl/corr_matrix.rdl
}

# ==============================================================================
proc doOnCreate {} {
  variable Sources
  addSources "Sources"
}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
    variable SimTop

    set SimTop { \
        tc_basic \
    }

    # Move data input to project path
    file copy -force "../sim/reforbit.txt" "../sim/bpmdata.txt" "../sim/respmat.txt" "../sim/corrout.txt" ${::fwfwk::PrjBuildPath}
}
